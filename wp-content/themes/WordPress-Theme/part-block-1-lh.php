<!-- Begin Block 1 -->
	<section class="block_1 linea-humana" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'block_1_lh' ); ?>
			</div>
		</div>
	</section>
<!-- End Block 1 -->