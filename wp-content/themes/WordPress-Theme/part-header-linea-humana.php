<!-- Begin Top -->
	<section class="top linea-humana" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-12 medium-3 columns text-center">
				<?php dynamic_sidebar( 'logo' ); ?>
			</div>
			<div class="small-12 medium-9 columns">
				<?php dynamic_sidebar( 'menu_lh' ); ?>
			</div>
		</div>
	</section>
<!-- End Top -->