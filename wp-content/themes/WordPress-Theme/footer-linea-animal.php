		<?php if ( is_page( array( 'inicio-linea-animal', 'nuestra-historia' ) ) ) : get_template_part( 'part', 'block-1-la' ); endif; ?>
		<?php if ( ! is_page( array( 'contactos' ) ) ) : get_template_part( 'part', 'block-2-la' ); endif; ?>
		<?php get_template_part( 'part', 'block-2' ); ?>
		<?php get_template_part( 'part', 'block-3' ); ?>
		<?php get_template_part( 'part', 'bottom' ); ?>
		<?php get_template_part( 'part', 'copyright' ); ?>
		<?php wp_footer(); ?>
	</body>
</html>